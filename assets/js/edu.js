var jq = jQuery.noConflict();
jq(document).ready(function(){
    jq('footer').load('footer.html');
    var swiper = new Swiper('#slideshow_header', {
      speed: 800,
      spaceBetween: 30,
      centeredSlides: true,
      autoplay: {
        delay: 3000,
        disableOnInteraction: false,
      },
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
    });
    var swiper1 = new Swiper('.main-banner, .sec-banner', {
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      pagination: {
        el: '.swiper-pagination',
      },
    });

    jq('.mobile-menu').click(function () {
      jq('.left-menu').toggleClass('show');
    });
    jq('.close-left').click(function () {
      jq('.left-menu').removeClass('show');
    });
    jq('#search-btns').click(function () {
      jq('.search-item').addClass('show');
      jq('.item-search').toggleClass('show');

    });
    jq('.search-icos').click(function () {
      jq('.search-item').removeClass('show');
      jq('.item-search').removeClass('show');
    });
    jq(".carousel").carousel({
      wrap: false
    });
    var like_before_you = jq("#per_psyb_page header input[name='psyb_like_before_you']").val(parseFloat(jq("#per_psyb_page header #fav_psyb var").text()));
    jq("#per_psyb_page header #fav_psyb").on('click', function() {
      jq(this).children("img").toggleClass("psyb_liked");
      if (jq(this).children("img").hasClass("psyb_liked")) {
        var plus_one_psyb = +jq(this).find("input").val() + 1;
        jq(this).find("input").val(plus_one_psyb);

        jq("#per_psyb_page header #fav_psyb img").fadeOut(300, function(){
          jq("#per_psyb_page header #fav_psyb img").attr("src", "img/like_white.png");
        }).fadeIn(300);
      }
      else {
        var min_one_psyb = +jq(this).find("input").val() - 1;
        jq(this).find("input").val(min_one_psyb);

        jq("#per_psyb_page header #fav_psyb img").fadeOut(300, function() {
          jq("#per_psyb_page header #fav_psyb img").attr("src", "img/like_trans.png");
        }).fadeIn(300);
      }
    });
    jq("#per_psyb_page article figcaption div a:first-child").click(function() {
      jq(this).children("img").toggleClass("liked_book");
      if (jq(this).children("img").hasClass("liked_book")) {
        var plus_one_book = +jq(this).find("input").val() + 1;
        jq(this).find("input").val(plus_one_book);

        jq(this).children("img").fadeOut(150, function() {
          jq(this).attr("src", "img/like_red.png");
        }).fadeIn(150);
        jq(this).find("input").val() + 1;
      }
      else {
        var min_one_book = +jq(this).find("input").val() - 1;
        jq(this).find("input").val(min_one_book);

        jq(this).children("img").fadeOut(150, function() {
          jq(this).attr("src", "img/Love@2x.png");
        }).fadeIn(150);
      }
    });
    jq("#per_psyb_page article .preview_book").click(function() {
      jq(this).parent().next().addClass("show_modal");
    });
    jq(".overlay_modal").click(function(e) {
      if (e.target !== this) return;
      jq(this).removeClass("show_modal");
    });
    jq("footer").load("C:/Users/sanch/Documents/slicing/edustore/footer.html");
});
